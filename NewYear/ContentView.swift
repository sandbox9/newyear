//
//  ContentView.swift
//  NewYear
//
//  Created by arai on 2019/12/27.
//  Copyright © 2019 Happy Co.,Ltd. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
